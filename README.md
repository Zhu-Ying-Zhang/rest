# Rest

https://www.jackrutorial.com/2018/02/spring-4-mvc-restful-web-services-json-crud-using-maven-within-the-eclipse-ide-and-mysql-database-tutorial.html

此為解決第一次使用Java來當後端Server的Start Kit，省去了過多XML的設定模式改用以Java來進行設定的方式。

目前是用以MAVEN的方式來管理依賴檔，可自行擴充需要之依賴，後續可能會以Gradle的方式來進行檔案的管理。

為了專案的方便度有開發幾個Utils可供參考並且使用，如果在使用上以及專案開發上有不方便以及想要的Utils可以提供給我來做參考並且上傳供大家使用。

之後也會慢慢新增我再開發所遇到的問題點以及解決方法，也希望大家能夠互相指教並且一起成長，謝謝。