#!/bin/bash
source ./setenv.sh

cd $PROJECT_HOME
mvn clean package
cd $PROJECT_HOME/target
cp $WAR_FILE $CATALINA_HOME/libexec/webapps/
cd $CATALINA_HOME/libexec/webapps/
mkdir $WAR_FOLDER
cd $WAR_FOLDER
jar xvf ../$WAR_FILE

