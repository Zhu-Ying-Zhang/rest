package springrest.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages="springrest")
@PropertySource(value = { "classpath:application.properties" })
public class WebConfig {
	/**
	 * 設置有關DB的連接也可進行其他設定
	 * 1. 請寫其他設定的範例
	 * 2. 註解清楚DB的設定為何
	 * 設定Bean的name是為了在Dao層可以利用該名稱直接對應到該設定
	 */
	@Autowired
	private Environment env;
	
	@Bean(name = "mysqlDb")
	public DataSource MydataSource(){
		DriverManagerDataSource Mydm = new DriverManagerDataSource();
		Mydm.setDriverClassName(env.getRequiredProperty("jdbc.driverClassName"));
		System.out.println(env.getRequiredProperty("jdbc.driverClassName"));
		Mydm.setUrl(env.getRequiredProperty("jdbc.url"));
		Mydm.setUsername(env.getRequiredProperty("jdbc.username"));
		Mydm.setPassword(env.getRequiredProperty("jdbc.password"));
		return Mydm;
	}
	
	@Bean(name = "mssqlDb")
	public DataSource MsdataSource(){
		DriverManagerDataSource Msdm = new DriverManagerDataSource();
		Msdm.setDriverClassName(env.getRequiredProperty("Msjdbc.driverClassName"));
		Msdm.setUrl(env.getRequiredProperty("Msjdbc.url"));
		Msdm.setUsername(env.getRequiredProperty("Msjdbc.username"));
		Msdm.setPassword(env.getRequiredProperty("Msjdbc.password"));
		return Msdm;
	}
	
	@Bean(name = "mysqlJdbcTemplate")
	public JdbcTemplate myJdbcTemplate(@Qualifier("mysqlDb") DataSource Mydm){
		JdbcTemplate MyJdbcTemplate = new JdbcTemplate(Mydm);
		MyJdbcTemplate.setResultsMapCaseInsensitive(true);
		return MyJdbcTemplate;
	}
	
	@Bean(name = "mssqlJdbcTemplate")
	public JdbcTemplate msJdbcTemplate(@Qualifier("mssqlDb") DataSource Msdm){
		JdbcTemplate MsJdbcTemplate = new JdbcTemplate(Msdm);
		MsJdbcTemplate.setResultsMapCaseInsensitive(true);
		return MsJdbcTemplate;
	}
	
//	@Bean(name = "mysqlDBUtil")
//	public DBUtil myDBUtil(@Qualifier("mysqlDb") DataSource Mydm){
//		DBUtil myDb = new DBUtil(Mydm);
//		return myDb;
//	}
//	
//	@Bean(name = "mssqlDBUtil")
//	public DBUtil msDbUtil(@Qualifier("mssqlDb") DataSource Msdm){
//		DBUtil msDb = new DBUtil(Msdm);
//		return msDb;
//	}
}
